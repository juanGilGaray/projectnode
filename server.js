//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  //res.send('hola mundo');
  res.sendFile(path.join(__dirname, 'index.html'));

});

app.get("/clientes/:idcliente",function(req,res){
  //res.send('hola mundo');
  res.send('cliente numero: '+req.params.idcliente);
});

app.post("/",function(req,res){
  res.send('hemos recibido su peticion cambiada');
});
app.put("/",function(req,res){
  res.send('hemos recibido su la actualizacion');
});
app.delete("/",function(req,res){
  res.send('hemos eliminado su registro');
});
